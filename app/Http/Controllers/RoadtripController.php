<?php

namespace App\Http\Controllers;

use App\Roadtrip;
use Illuminate\Http\Request;

class RoadtripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Roadtrip  $roadtrip
     * @return \Illuminate\Http\Response
     */
    public function show(Roadtrip $roadtrip)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Roadtrip  $roadtrip
     * @return \Illuminate\Http\Response
     */
    public function edit(Roadtrip $roadtrip)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Roadtrip  $roadtrip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Roadtrip $roadtrip)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Roadtrip  $roadtrip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Roadtrip $roadtrip)
    {
        //
    }
}
