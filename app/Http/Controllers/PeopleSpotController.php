<?php

namespace App\Http\Controllers;

use App\peopleSpot;
use Illuminate\Http\Request;

class PeopleSpotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\peopleSpot  $peopleSpot
     * @return \Illuminate\Http\Response
     */
    public function show(peopleSpot $peopleSpot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\peopleSpot  $peopleSpot
     * @return \Illuminate\Http\Response
     */
    public function edit(peopleSpot $peopleSpot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\peopleSpot  $peopleSpot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, peopleSpot $peopleSpot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\peopleSpot  $peopleSpot
     * @return \Illuminate\Http\Response
     */
    public function destroy(peopleSpot $peopleSpot)
    {
        //
    }
}
