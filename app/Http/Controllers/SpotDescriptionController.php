<?php

namespace App\Http\Controllers;

use App\SpotDescription;
use Illuminate\Http\Request;

class SpotDescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SpotDescription  $spotDescription
     * @return \Illuminate\Http\Response
     */
    public function show(SpotDescription $spotDescription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpotDescription  $spotDescription
     * @return \Illuminate\Http\Response
     */
    public function edit(SpotDescription $spotDescription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SpotDescription  $spotDescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpotDescription $spotDescription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpotDescription  $spotDescription
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpotDescription $spotDescription)
    {
        //
    }
}
