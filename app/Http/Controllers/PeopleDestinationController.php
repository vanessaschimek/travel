<?php

namespace App\Http\Controllers;

use App\PeopleDestination;
use Illuminate\Http\Request;

class PeopleDestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PeopleDestination  $peopleDestination
     * @return \Illuminate\Http\Response
     */
    public function show(PeopleDestination $peopleDestination)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PeopleDestination  $peopleDestination
     * @return \Illuminate\Http\Response
     */
    public function edit(PeopleDestination $peopleDestination)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PeopleDestination  $peopleDestination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PeopleDestination $peopleDestination)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PeopleDestination  $peopleDestination
     * @return \Illuminate\Http\Response
     */
    public function destroy(PeopleDestination $peopleDestination)
    {
        //
    }
}
