<?php

namespace App\Http\Controllers;

use App\DestinationDescription;
use Illuminate\Http\Request;

class DestinationDescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DestinationDescription  $destinationDescription
     * @return \Illuminate\Http\Response
     */
    public function show(DestinationDescription $destinationDescription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DestinationDescription  $destinationDescription
     * @return \Illuminate\Http\Response
     */
    public function edit(DestinationDescription $destinationDescription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DestinationDescription  $destinationDescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DestinationDescription $destinationDescription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DestinationDescription  $destinationDescription
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestinationDescription $destinationDescription)
    {
        //
    }
}
