<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DestinationDescription extends Model
{
    protected $table='destination_description';
    protected $fillable = [
        'description'
    ];
}
