<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpotDescription extends Model
{
    protected $table='spot_description';
    protected $fillable = [
        'description'
    ];
}
