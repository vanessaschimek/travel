<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleSpotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people_spot', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('spot_id')->unsigned();
            $table->integer('people_id')->unsigned();
            $table->longText('description')->nullable();
            $table->timestamps();

            $table->foreign('spot_id')
                ->references('id')->on('spot')
                ->onUpdate('cascade')
                ->onDelete('no action');

            $table->foreign('people_id')
                ->references('id')->on('people')
                ->onUpdate('cascade')
                ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people_spot');
    }
}
