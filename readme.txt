PowerShell
# $laravel new travel
# $npm init
# $npm install
# $php artisan make:auth
# $php artisan make:model Country -r
https://www.cloudways.com/blog/laravel-vue-single-page-app/

Migrationen
php artisan make:migration create_category_table --create=category
php artisan make:migration create_destination_table --create=destination
php artisan make:migration create_category_destination_table --create=category_destination
php artisan make:migration create_destination_description_table --create=destination_description
php artisan make:migration create_people_table --create=people
php artisan make:migration create_people_destination_table --create=people_destination
php artisan make:migration create_spot_table --create=spot
php artisan make:migration create_roadtrip_table --create=roadtrip
php artisan make:migration create_spot_description_table --create=spot_description
php artisan make:migration create_video_table --create=video
php artisan make:migration create_picture_table --create=picture
php artisan make:migration create_recipe_table --create=recipe
php artisan make:migration create_people_spot_table --create=people_spot

Models
php artisan make:model Category -r
php artisan make:model Destination -r
php artisan make:model DestinationDescription -r
php artisan make:model People -r
php artisan make:model PeopleDestination -r
php artisan make:model Spot -r
php artisan make:model Roadtrip -r
php artisan make:model SpotDescription -r
php artisan make:model Video -r
php artisan make:model Recipe -r
php artisan make:model peopleSpot -r
